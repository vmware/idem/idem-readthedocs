def check_api_response(hub, ctx, api_response):
    """
    Verify readthedocs api response data; manage logging and errors

    https://docs.readthedocs.io/en/stable/api/v3.html
    """
    try:
        assert api_response.status_code == 200
        return api_response.json()
    except AssertionError as debug_error:
        error_message = [
            f"HTTP {api_response.status_code} Error: {api_response.json()['detail']}"
        ]
        if api_response.status_code == 403:
            error_message.append(
                f"Confirm that project '{ctx['acct']['project_name']}' is a valid project name, and that the API token has permissions to manage it."
            )
        hub.log.debug(debug_error, exc_info=True)
        hub.log.error(error_message)
