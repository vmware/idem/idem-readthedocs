def pre_get(hub, ctx):
    kwargs = ctx.get_arguments()


def post_get(hub, ctx):
    kwargs = ctx.get_arguments()
    # do postprocessing on the return of "get"
    ret = ctx.ret
    return ret


def call_get(hub, ctx):
    return ctx.func(*ctx.args, **ctx.kwargs)


def post_trigger(hub, ctx):
    kwargs = ctx.get_arguments()
    # do postprocessing on the return of "trigger"
    ret = ctx.ret
    return ret
